-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2015 at 02:46 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `web_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `hall`
--

CREATE TABLE IF NOT EXISTS `hall` (
  `id` int(100) NOT NULL,
  `images` varchar(200) NOT NULL,
  `space` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `ranking` int(30) NOT NULL,
  `review` varchar(500) NOT NULL,
  `rate` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `commitments` varchar(200) NOT NULL,
  `slideImg1` varchar(100) NOT NULL,
  `slideImg2` varchar(100) NOT NULL,
  `slideCont1` varchar(10) NOT NULL,
  `slideCont2` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hall`
--

INSERT INTO `hall` (`id`, `images`, `space`, `location`, `ranking`, `review`, `rate`, `name`, `commitments`, `slideImg1`, `slideImg2`, `slideCont1`, `slideCont2`) VALUES
(1, 'images/halls1.jpg', '500', 'Town Ship', 9, 'This hall was beautifull and convinient for my daughter marriage', 25000, 'Marijuana', 'We make your day.', 'images/halls1.jpg', 'images/halls1.jpg', 'Barat', 'Mehndi'),
(2, 'images/halls3.jpg', '300', 'Eden Town', 6, 'This was the best hall I have ever seen.', 37000, 'Eden Complex ', 'We make you smile', 'images/halls1.jpg', 'images/halls7.jpg', 'Barat', 'We make you smile. '),
(3, 'images/halls2.jpg', '300', 'Model Town', 6, 'This was the best hall I have ever seen.', 15000, 'Gourmet Palace  ', 'We make you smile', 'images/halls11.jpg', 'images/halls12.jpg', 'Barat', 'We make you smile. '),
(4, 'images/halls4.jpg', '400', 'Lal Pul', 6, 'This was the best hall I have ever seen.', 15000, 'Shaheen Palace  ', 'We make you smile', 'images/halls9.jpg', 'images/halls7.jpg', 'Barat', 'We make you smile. '),
(5, 'images/halls5.jpg', '600', 'Lovely Town', 8, 'This was the best hall I have ever seen.', 20000, 'Lovely Complex ', 'We make you smile', 'images/halls4.jpg', 'images/halls7.jpg', 'Barat', 'We make you smile. '),
(6, 'images/halls6.jpg', '200', 'Wonder Town', 4, 'This was the best hall I have ever seen.', 12000, 'Lal pul Complex ', 'We make you smile', 'images/halls1.jpg', 'images/halls12.jpg', 'Barat', 'We make you smile. '),
(7, 'images/halls7.jpg', '200', 'Alo Town', 4, 'This was the best hall I have ever seen.', 12000, 'Pyaza Complex ', 'We make you smile', 'images/halls11.jpg', 'images/halls12.jpg', 'Barat', 'We make you smile. '),
(8, 'images/halls8.jpg', '500', 'Town Ship', 9, 'This hall was beautifull and convinient for my daughter marriage', 25000, 'Marijuana', 'We make your day.', 'images/halls1.jpg', 'images/halls1.jpg', 'Barat', 'Mehndi'),
(9, 'images/halls9.jpg', '500', 'Town Ship', 9, 'This hall was beautifull and convinient for my daughter marriage', 25000, 'Marijuana', 'We make your day.', 'images/halls1.jpg', 'images/halls1.jpg', 'Barat', 'Mehndi');

-- --------------------------------------------------------

--
-- Table structure for table `photographer`
--

CREATE TABLE IF NOT EXISTS `photographer` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rate` int(100) NOT NULL,
  `rating` int(100) NOT NULL,
  `images` varchar(200) NOT NULL,
  `review` varchar(100) NOT NULL,
  `slideImg1` varchar(100) NOT NULL,
  `slideImg2` varchar(100) NOT NULL,
  `slidecont1` varchar(100) NOT NULL,
  `slidecont2` varchar(100) NOT NULL,
  `nameDj` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photographer`
--

INSERT INTO `photographer` (`id`, `name`, `rate`, `rating`, `images`, `review`, `slideImg1`, `slideImg2`, `slidecont1`, `slidecont2`, `nameDj`) VALUES
(1, 'Mama Gama Studios', 2000, 9, 'images/camera.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Junaid JJ DJ'),
(2, 'Lama Gama Studios', 2000, 6, 'images/camera2.png', 'His sound system was awesome. ', 'images/photoimg1.jpg', 'images/photoimg.jpg', 'We capture your memories', 'We capture your memories', 'Awais DJ'),
(3, 'Tama Gama Studios', 1500, 5, 'images/camera3.png', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'JJ DJ'),
(4, 'Alo Studios', 1500, 5, 'images/camera6.png', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'JJ DJ'),
(5, 'Amma Gama Studios', 2000, 9, 'images/camera5.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Junaid DJ'),
(6, 'Mama Gama Studios', 2000, 9, 'images/camera7.png', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Junaid JJ DJ'),
(7, 'Gama Studios', 2000, 9, 'images/camera8.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Faizi Dj'),
(8, 'Gama Studios', 2000, 9, 'images/camera9.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Annoy Dj'),
(9, 'Gama Studios', 2000, 9, 'images/camera10.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Dj Mewao');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hall`
--
ALTER TABLE `hall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photographer`
--
ALTER TABLE `photographer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hall`
--
ALTER TABLE `hall`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `photographer`
--
ALTER TABLE `photographer`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
