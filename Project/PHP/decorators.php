<?php include('conn.php'); ?>
<html>
  <head>
      
      <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
	<title>Trio-Decorators</title>
	
   <link rel='stylesheet' href='./bootstrap-3.3.5-dist/bootstrap-combined.min.css'/>
                          
    

        <script type="text/javascript" src="jquery/jquery-1.11.3.js" ></script>

    <link href="css/app.css" rel="stylesheet" type="text/css">

<script src="bootstrap-3.3.5-dist/js/jQuery.min.js"></script>

    
<!-- Latest compiled JavaScript -->
    <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

<!-- adding files of sidebar --->
   <script async="" src="javascript/analytics.js"></script>
      <script type="text/javascript" src="javascript/jquery.js"></script>
    
         <script src="jquery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    
$(document).ready(function(){
    
    function showRoom(){
        
        $.ajax({
            type:"POST",
            url:"showall.php",
            data:{action:"show"},
            success:function(data){
                $("#content2").html(data);
            }
        });
    }
    showRoom();
});
    
</script>
           <script type="text/javascript">
               
              
    
        $(document).ready(function2); 
          function function2() {
              
              
    var a = function () {
        var b = $(window).scrollTop();
        var d = $("#contentWrapper").offset().top;
        var f = $("#footer").offset();
        var c = $("#contentLeft");
        
        var h = $("#contentLeft").height() + 110;// margin
      

        if (b > d) {
            var myTop = $(window).scrollTop();
            if (myTop > f - h) myTop = f - h;
            c.css({
                
                position: "absolute",
                top: myTop,
                bottom: ""
            })
        } else {
        
            if (b <= d) {
                c.css({
                    position: "fixed",
                    top: 0,
                    left:0
                })
            }
        }
    };
    $(window).scroll(a);
    a()

                       
          
}
        
//Open the dialog box when the button is clicked.

          
</script>
      
    <!-- social bar linking file-->

  
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="jQuery-Plugin-For-Floating-Social-Share-Contact-Sidebar/css/contact-buttons.css">
   
    <script src="jQuery-Plugin-For-Floating-Social-Share-Contact-Sidebar/js/jquery.contact-buttons.js"></script>
    <script src="jquery.js"></script>
     
   

    <!--   end of socail bar -->
      

 
    	<style type="text/css" media="all">@import "css/header.css";</style> 
          <link rel='stylesheet' href='./js/bootstrap.min.js'/>
       <link rel='stylesheet' href='./css/style-decorators-list.css'/>
   <link rel='stylesheet' href='./CSS/popup.css'/>
   <link rel='stylesheet' href='./css/style-decorators.css'/>    
      <link href="./css/style-decorators-list.css" rel="stylesheet" type="text/css">   
      <link rel='stylesheet' href='./CSS/footer.css'/>   
       <link rel='stylesheet' href='./css/style-decorators.css'/>  
    <link rel="stylesheet" href="./Content Slider_files/contentStyle.css">	
       <link rel='stylesheet' href='./CSS/popup2.css'/>
      
      
<style>
    /* Global */
body {
    background: #a83b3b;
    padding: 40px;
}  

div { word-break: break-all; }

img { max-width:100%; }

a {
	-webkit-transition: all 150ms ease;
	-moz-transition: all 150ms ease;
	-ms-transition: all 150ms ease;
	-o-transition: all 150ms ease;
	transition: all 150ms ease; 
	}
    a:hover {
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)"; /* IE 8 */
        filter: alpha(opacity=50); /* IE7 */
        opacity: 0.6;
        text-decoration: none;
    }


/* Container */
.container-fluid {
    background: white;
    margin: 40px auto 10px;
    padding: 20px 0px;
    max-width: 860px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
}


/* Page Header */
.page-header {
    background: white;
    margin: -60px 0px 0px;
    border-top: 4px solid #ccc;
    color: black;
    text-transform: uppercase;
    }
    .page-header h3 {
        line-height: 0.88rem;
        color: #a83b3b;
        }



/* Thumbnail Box */
.caption {
    height: 30px;
    width: 100%;
    margin: 20px 0px;

    box-sizing:border-box;
    -moz-box-sizing:border-box;
    -webkit-box-sizing:border-box;
}
.caption .span4, .caption .span8 {
    padding: 0px 20px;
}
.caption .span4 {
    border-right: 1px dotted #CCCCCC;
}
.caption h3 {
    color: #a83b3b;
    line-height: 2rem;
    margin: 0 0 20px;
    text-transform: uppercase;
    }
    .caption p {
        font-size: 1rem;
        line-height: 1.6rem;
        color: #a83b3b;
        }
        .btn.btn-mini {
            background: #a83b3b;
            border-radius: 0 0 0 0;
            color: #fbf4e0;
            font-size: 0.63rem;
            text-shadow: none !important;
            }
.carousel-control {
    top: 33%;
}




/* Footer */
.footer {
    margin: auto;
    width: 100%;
    max-width: 960px;
    display: block;
    font-size: 0.69rem;
    }
    .footer, .footer a {
        color: #fff;
        }
        p.right  { 
            float: right; 
            }




/* ADD-ON
-------------------------------------------------- */
body:after{content:"less than 320px";font-size:1rem;font-weight:bold;position:fixed;bottom:0;width:100%;text-align:center;background-color:hsla(1,60%,40%,0.7);color:#fff;height:20px;padding-top:0;margin-left:0;left:0}@media only screen and (min-width:320px){body:after{content:"320 to 480px";background-color:hsla(90,60%,40%,0.7);height:20px;padding-top:0;margin-left:0}}@media only screen and (min-width:480px){body:after{content:"480 to 768px";background-color:hsla(180,60%,40%,0.7);height:20px;padding-top:0;margin-left:0}}@media only screen and (min-width:768px){body:after{content:"768 to 980px";background-color:hsla(270,60%,40%,0.7);height:20px;padding-top:0;margin-left:0}}@media only screen and (min-width:980px){body:after{content:"980 to 1024px";background-color:hsla(300,60%,40%,0.7);height:20px;padding-top:0;margin-left:0}}@media only screen and (min-width:1024px){body:after{content:"1024 and up";background-color:hsla(360,60%,40%,0.7);height:20px;padding-top:0;margin-left:0}}

::selection { background: #ff5e99; color: #FFFFFF; text-shadow: 0; }
::-moz-selection { background: #ff5e99; color: #FFFFFF; }

a, a:focus, a:active, a:hover, object, embed { outline: none; }
:-moz-any-link:focus { outline: none; }
input::-moz-focus-inner { border: 0; }
      </style>
      
       <script>
// Carousel Auto-Cycle
  $(document).ready(function() {
    $('.carousel').carousel({
      interval: 6000
    })
  });

    </script>
  </head>
      
  <body>
    
  
<div style="position:absolute;top:0;left:0;z-index:99999;background-color:black;border-radius:10px">
    <img src="./images/logo.gif" style="width:200px;height:50px;z-index:678"></img></div>    

           
  <?php include("header.php");?>
       
      <div id="contentWrapper"></div>
      <div class = "socio-bar">
          
        
            
        <!--ye lo social bar -->
 
       
    <div id="contact-buttons-bar" class="slide-on-scroll" data-top="120px" style="right: 20px;">
        <a href="http://www.facebook.com/jqueryscript" class="contact-button-link cb-ancor facebook" title="Follow on Facebook" target="_blank">
            <span class="fa fa-facebook"></span></a>
        <a href="https://www.linkedin.com/company/mycompany" class="contact-button-link cb-ancor linkedin" title="Visit on LinkedIn"><span class="fa fa-linkedin"></span></a>
        <a href="https://plus.google.com/109366824274411741515" class="contact-button-link cb-ancor gplus" title="Visit on Google Plus"><span class="fa fa-google-plus"></span></a>
        <a href="http://github.com" class="contact-button-link cb-ancor git" title="My title for the button"><span class="fa fa-github"></span></a>
        <a href="tel:+000" class="contact-button-link cb-ancor phone separated" title="Call us"><span class="fa fa-phone"></span></a>
        <a href="mailto:test@web.com" class="contact-button-link cb-ancor email" title="Send us an email"><span class="fa fa-envelope"></span></a></div>
        
        
        <!-- social bar ka end-->
    
          </div>
      
      
    
    
      <div class="main-content">    
  
                
       
      <div id="description">
      
          
      <div id="decorator-des" style="padding-top:50px">
          
          
         <div class="container-fluid" style='width:855px;height:450px'>
<div class="row-fluid">
<div class="span12">

    <div class="page-header">
       
       <h1> <p>Our Top Decorators </p></h1>
    </div>
        
    <div class="carousel slide" id="myCarousel" style='width:855px;height:450px'>
        <div class="carousel-inner" style='width:855px;height:450px'>
 
  

               <?php 

                                                    $query = "SELECT * FROM decorators";
                                                    $sql =mysqli_query($conn, $query);

                                                    $recordsFound = mysqli_num_rows($sql);

                                                        $a=0;
                                                    while($row = mysqli_fetch_array($sql))

                                                    {
                                                        if($a==0){
             echo "<div class='item active' style='width:855px;height:450px' >
            
                <div class='bannerImage'>
                    <a href='#'><img src='./images/EventDesigners/{$row['image']}.jpg' style='width:855px;height:340px'></a>
                </div>
                            
                <div class='caption row-fluid'>
                    <div class='span4'><h3> </h3>
                    <a class='info' href='#'> {$row['name']}</a>
                    </div>                	
                	<div class='span8'><p>
                     {$row['specialization']}
                    </p>
                	</div>
                </div>
                                 
            </div><!-- /Slide1 -->; ";
                      
                
                
           
                                                            
                                                        }
                                                        if($a==5)
                                                        {
                                                            break;
                                                        }
                                                        else if($a!=0)
                                                        {
                                                            
             echo "<div class='item' style='width:855px;height:450px' >
            
                <div class='bannerImage'>
                    <a href='#'><img src='./images/EventDesigners/{$row['image']}.jpg' style='width:855px;height:340px'></a>
                </div>
                            
                <div class='caption row-fluid'>
                    <div class='span4'><h3> {$row['name']} </h3>
                    <a class='info' href='#'>  {$row['name']}</a>
                    </div>                	
                	<div class='span8'><p>
                     {$row['specialization']}
                    </p>
                	</div>
                </div>
                                 
            </div><!-- /Slide1 -->; ";
                                                        }
                                                            $a++;

                                                    }

                                                    


                                               ?>
        <div class="control-box">                            
            <a data-slide="prev" href="#myCarousel" class="carousel-control left">‹</a>
            <a data-slide="next" href="#myCarousel" class="carousel-control right">›</a>
        </div><!-- /.control-box -->   
                              
    </div><!-- /#myCarousel -->
        
</div><!-- /.span12 -->          
</div><!-- /.row --> 
</div><!-- /.container -->


  

          </div>
          </div>
      
      
   	<!-- Container for Slider, Navigation and Pagination Controls -->
		
          <div class="gallery-container">			
			<div class="gallery">
				<h2 style="font-family:courier">Our Top Decorators</h2>
				<!-- The actual moving Slider -->
				<div class="slider" >
					<ul style="margin-left: -1150px;">
						<li>
							<div class="slides">
								<div class="slides_content">
									  	
    <div id="content2" style="padding-left:50px">
    
    </div>
                 
                                    
                                    
                                    </div>
								
							</div>
						</li>

					<li>
							<div class="slides">
								<div class="slides_content">


                                     <div id= "decorators">
                                              <?php 

                                                    $query = "SELECT * FROM decorators";
                                                    $sql =mysqli_query($conn, $query);

                                                    $recordsFound = mysqli_num_rows($sql);

                                                        $a=0;
                                                    while($row = mysqli_fetch_array($sql))

                                                    {
                                                        if($a==5)
                                                        {

                                                        }
                                                        else 
                                                        {

                                                        echo "  <div class='deco'>";
                                                        echo " <div class='view view-seventh'>";
                                                        echo "<img src='./images/EventDesigners/{$row['image']}.jpg' align='honey-sing' >";                  
                                                        echo "<div class='mask'>";                      
                                                        echo " <h1 style='color:black'> {$row['name']} </h1><a href='' class='info' style='pointer-events: none;
                                       cursor: default;background-color:blue;'> Charges : {$row['charges']}</a>
                                                        <p> {$row['specialization']} </p>   
                                                        <a href='#{$row['id']}' class='info'>Read More</a> 
                                                               
                                                        <br> <br>
                                                            </div>  </div>	</div><div id='{$row['id']}' class='modalDialog'><div><a href='#close' title='Close' class='close'>X</a>
                                                            <h2 style='background-color:red;width:200px;border-radius: 40px;padding-left:20px;display:'inline'; '>Decorator Description</h2>
                                                             <p><h3> Rating :{$row['rating']}/5 </h3></p>  


                                                            <p> {$row['detail']} </p>  
                                                                </div>
                                                                </div>	
                                                               


                                                                
                                                                
                                                                ";

                                                            $a++;

                                                    }

                                                    }


                                               ?>
                                              </div>
                                    
                                    </div>
								</div>
						
						</li>
						
						
						
					</ul>
				</div>
				
				<!-- Navigation Button Controls -->
				
				<div class="slider-nav" style="display: none;">
			
				</div>
					
			</div>
			<!-- Pagination Controls -->
			<div class="slider-pagi">
				<div class="pagi-container">
					<ul><li><a href="decorators.php" data-pgno="0" style="border: none;">1</a></li><li><a href="decorators.php" data-pgno="1" style="border: none;">2</a></li><li><a href="./Content Slider_files/Content Slider.html" data-pgno="2" style="border: 2px solid grey; font-weight: 900; font-size: 18px;">3</a></li></ul>	<!-- Here Paginations will be dynamically created, depending on Number of elements in the list. -->					
				</div>
			</div>
		</div>
		
		





      
      
          
          </div>

      <!----This is the Side Bar from below------->

  <div id="contentLeft" style="margin-top:130px;font-family:'Century Gothic';font-size:20px">

        <ul id="leftNavigation">

            <li class="active">
                <a href="homepage-v2.php"><i class="fa fa-coffee leftNavIcon"></i> Home</a>
                
            </li>
            <li>
                <a href=""><i class="fa fa-flask leftNavIcon"></i> Navigate To</a>
                <ul>
                    <li>
                        <a href="djSection.php"><i class="fa fa-angle-right leftNavIcon"></i> DJ-Section</a>
                    </li>
                    <li>
                        <a href="hall.php"><i class="fa fa-angle-right leftNavIcon"></i> Halls</a>
                    </li>
                    <li>
                        <a href="decorators.php"><i class="fa fa-angle-right leftNavIcon"></i> Decorators</a>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-angle-right leftNavIcon"></i> Caterers</a>
                    </li>
                    <li>
                        <a href="photographer.php"><i class="fa fa-angle-right leftNavIcon"></i> Photographers</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-truck leftNavIcon"></i> About Us</a>
                <ul>
                    <li>
                        <a href="aboutUs.php"><i class="fa fa-angle-right leftNavIcon"></i> Who we are.</a>
                    </li>
                    <li>
                        <a href="testimonial.php"><i class="fa fa-angle-right leftNavIcon"></i> What we have done</a>
                    </li>
                    
                </ul>
            </li>
            <li class="clickable">
                <a href="contact.php"><i class="fa fa-envelope-o leftNavIcon"></i> Contact us</a>
            </li>


        </ul>

    </div>


    <!--The Side-Bar ends here-->

          
      <!-- Loading JavaScript Codes. -->
		<script src="Content%20Slider_files/jquery-2.1.0.js"></script>
		<script src="Content Slider_files/contentSliderScript.js"></script>
		<script>
			// 
			jQuery(document).ready(function ($) {
				// creating a container variable to hold the 'UL' element. It uses method chaining.
				var container=$('div.slider')
											.css('overflow','hidden')
											.children('ul');
				
				// creating pagination variable which holds the 'UL' element.
				var pagicontainer=$('div.pagi-container').children('ul');
				
				/* 
				On the event of mouse-hover, 
					i) Change the visibility of Button Controls.
					ii) SET/RESET the "intv" variable to switch between AutoSlider and Stop mode.
				*/
				$('.gallery').hover(function( e ){
					$('.slider-nav').toggle();
					return e.type=='mouseenter'?clearInterval(intv):autoSlider();
				});
				
				// Creating the 'slider' instance which will set initial parameters for the Slider.
				var sliderobj= new slider(container,pagicontainer,$('.slider-nav'));
				/*
				This will trigger the 'setCurrentPos' and 'transition' methods on click of any button
				 "data-dir" attribute associated with the button will determine the direction of sliding.
				*/
				sliderobj.nav.find('button').on('click', function(){
					sliderobj.setCurrentPos($(this).data('dir'));
					sliderobj.transition();
				});
				
				/*
				This will trigger the 'setCurrentPos' and 'transition' methods on click of any Pagination icons.
				 "data-pgno" attribute associated with the Pagination icons will determine the value of current variable.
				*/
				sliderobj.pagicontainer.find('li a').on('click', function(){
					sliderobj.setCurrentPos($(this).data('pgno'));
					sliderobj.transition();					
				});
				
				autoSlider(); // Calling autoSlider() method on Page Load.
				
				/* 
				This function will initialize the interval variable which will cause execution of the inner function after every 3 seconds automatically.
				*/
				function autoSlider()
				{
					return intv = setInterval(function(){
						sliderobj.setCurrentPos('next');
						sliderobj.transition();
					}, 5000);
				}
				
			});
		</script>
	

   
    
      <div class="clearfix"></div>
    
  <iframe id="footer" src="footer.html" style="width:1345px;height:330px;border:none" scrolling="no"></iframe>
    
      
      
      

  </body>
</html>