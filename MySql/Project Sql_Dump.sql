-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2016 at 08:42 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `caterers`
--

CREATE TABLE IF NOT EXISTS `caterers` (
  `id` int(20) NOT NULL,
  `image` varchar(200) NOT NULL,
  `hotel` varchar(100) NOT NULL,
  `availability` int(1) NOT NULL DEFAULT '1',
  `title` varchar(20) NOT NULL,
  `members` int(3) NOT NULL,
  `charges` int(20) NOT NULL,
  `region` varchar(500) NOT NULL,
  `rating` int(1) NOT NULL DEFAULT '5',
  `comments` varchar(1000) NOT NULL DEFAULT 'Satisfied by their services.',
  `foodcategory` varchar(500) NOT NULL,
  `experience` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `caterers`
--

INSERT INTO `caterers` (`id`, `image`, `hotel`, `availability`, `title`, `members`, `charges`, `region`, `rating`, `comments`, `foodcategory`, `experience`) VALUES
(1, '1', 'Avari', 1, 'Avari', 7, 4000, 'johar town', 3, 'Nice Caterers trust sellers ', 'Desi', 36),
(2, '2', 'Pearl', 1, 'Avari', 3, 43334, 'johar town', 4, 'Nice Caterers trust sellers ', 'Desi', 4),
(3, '3', 'Pearl', 1, 'Avari', 4, 3333, 'johar town', 2, 'Nice Caterers trust sellers ', 'Desi', 7),
(4, '4', 'Gourmet', 1, 'Avari', 8, 77777, 'johar town', 5, 'Nice Caterers trust sellers ', 'Desi', 43),
(5, '5', 'Awaisiya', 1, 'Avari', 22, 4000, 'johar town', 1, 'Nice Caterers trust sellers ', 'Desi', 2),
(6, '6', 'Telnet', 1, 'Avari', 7, 77755, 'johar town', 4, 'Nice Caterers trust sellers ', 'Desi', 31),
(7, '7', 'Avari', 1, 'Avari', 56, 4000, 'johar town', 4, 'Nice Caterers trust sellers ', 'Desi', 3),
(8, '8', 'Continental', 1, 'Avari', 4, 34556, 'johar town', 2, 'Nice Caterers trust sellers ', 'Desi', 32),
(9, '9', 'jinnah', 1, 'Avari', 7, 47765, 'johar town', 3, 'Nice Caterers trust sellers ', 'Desi', 5),
(10, '10', 'super', 1, 'Avari', 76, 546456, 'johar town', 1, 'Nice Caterers trust sellers ', 'Desi', 1),
(11, '11', 'Avari', 1, 'Avari', 6, 4000, 'johar town', 3, 'Nice Caterers trust sellers ', 'Desi', 3);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `ID` int(5) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Contact` varchar(15) NOT NULL,
  `Email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`ID`, `Name`, `Contact`, `Email`) VALUES
(1, 'junaid', '4567890', 'junaid@gmail'),
(2, 'asad', '1234', 'asad@gmail'),
(3, 'asad', '1234', 'asad@gmail'),
(6, 'hdr', '123678', 'hdr@gmail');

-- --------------------------------------------------------

--
-- Table structure for table `decorators`
--

CREATE TABLE IF NOT EXISTS `decorators` (
  `id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rating` int(1) NOT NULL,
  `specialization` varchar(500) NOT NULL,
  `image` varchar(200) NOT NULL,
  `detail` varchar(1000) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `charges` int(20) NOT NULL,
  `availability` int(1) NOT NULL DEFAULT '1',
  `region` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `decorators`
--

INSERT INTO `decorators` (`id`, `name`, `rating`, `specialization`, `image`, `detail`, `comments`, `charges`, `availability`, `region`) VALUES
(1, 'awais', 4, ' We offer expertise that you might be looking forward to for your next big professional event or family celebration.', '1', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 30000, 1, 'lahore'),
(2, 'Ali Khaadi', 3, 'Expertise that you might be looking forward to for your next big professional event or family celebration.', '2', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 30000, 1, 'lahore'),
(3, 'Khaadi', 5, 'Expertise that you might be looking forward to for your next big professional event or family celebration.', '3', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 23000, 1, 'lahore'),
(4, 'Arif', 2, 'Expertise that you might be looking forward to for your next big professional event or family celebration.', '4', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 33000, 1, 'lahore'),
(5, 'Arif', 5, 'Expertise that you might be looking forward to for your next big professional event or family celebration.', '5', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 33000, 1, 'lahore'),
(6, 'Arif', 5, 'Expertise that you might be looking forward to for your next big professional event or family celebration.', '6', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 33000, 1, 'lahore'),
(7, 'awais', 4, ' We offer expertise that you might be looking forward to for your next big professional event or family celebration.', '7', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 30000, 1, 'lahore'),
(8, 'awais', 4, ' We offer expertise that you might be looking forward to for your next big professional event or family celebration.', '8', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 30000, 1, 'lahore'),
(9, 'awais', 4, ' We offer expertise that you might be looking forward to for your next big professional event or family celebration.', '9', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 30000, 1, 'lahore'),
(10, 'awais', 4, ' We offer expertise that you might be looking forward to for your next big professional event or family celebration.', '10', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 30000, 1, 'lahore'),
(11, 'awais', 4, ' We offer expertise that you might be looking forward to for your next big professional event or family celebration.', '11', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 30000, 1, 'lahore'),
(12, 'awais', 4, ' We offer expertise that you might be looking forward to for your next big professional event or family celebration.', '12', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 30000, 1, 'lahore'),
(13, 'Khaadi', 5, 'Expertise that you might be looking forward to for your next big professional event or family celebration.', '13', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 23000, 1, 'lahore'),
(14, 'Khaadi', 5, 'Expertise that you might be looking forward to for your next big professional event or family celebration.', '14', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 23000, 1, 'lahore'),
(15, 'Khaadi', 5, 'Expertise that you might be looking forward to for your next big professional event or family celebration.', '15', 'We are planning mega concert for Honey Singh Fans in Pakistan. The dates will be announce shortly.s all time ready to handle events of intense level. We offer expertise that you might be looking forward to for your next big professional event or family celebration.', 'Nice', 23000, 1, 'lahore');

-- --------------------------------------------------------

--
-- Table structure for table `dj`
--

CREATE TABLE IF NOT EXISTS `dj` (
  `ID` int(5) NOT NULL,
  `DjName` varchar(20) NOT NULL,
  `Ratings` int(1) NOT NULL,
  `Rate` int(10) NOT NULL,
  `Music` varchar(100) NOT NULL,
  `DjImage` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dj`
--

INSERT INTO `dj` (`ID`, `DjName`, `Ratings`, `Rate`, `Music`, `DjImage`) VALUES
(1, 'DJ-Bilal', 5, 12000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj3.jpg'),
(2, 'DJ-Ahmed', 5, 14000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj2.jpg'),
(3, 'DJ-Sheroz', 4, 14000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj1.jpg'),
(4, 'DJ-Ali', 4, 13000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj4.jpg'),
(5, 'DJ-Sultan', 4, 13000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj5.jpg'),
(6, 'DJ-Zain', 4, 13000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj5.jpg'),
(7, 'DJ-Butt', 3, 10000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj1.jpg'),
(8, 'DJ-Shaheen', 3, 10000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj2.jpg'),
(9, 'DJ-Shavez', 3, 10000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj3.jpg'),
(10, 'DJ-Hamza', 3, 10000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj4.jpg'),
(11, 'DJ-Awais', 3, 10000, 'john_wick_trailer_music_break_science_brain_reaction_feat._redman_and_lettuce_mp3_55288.mp3', 'dj5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hall`
--

CREATE TABLE IF NOT EXISTS `hall` (
  `id` int(100) NOT NULL,
  `images` varchar(200) NOT NULL,
  `space` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `ranking` int(30) NOT NULL,
  `review` varchar(500) NOT NULL,
  `rate` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `commitments` varchar(200) NOT NULL,
  `slideImg1` varchar(100) NOT NULL,
  `slideImg2` varchar(100) NOT NULL,
  `slideCont1` varchar(10) NOT NULL,
  `slideCont2` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hall`
--

INSERT INTO `hall` (`id`, `images`, `space`, `location`, `ranking`, `review`, `rate`, `name`, `commitments`, `slideImg1`, `slideImg2`, `slideCont1`, `slideCont2`) VALUES
(1, 'images/halls1.jpg', '500', 'Town Ship', 9, 'This hall was beautifull and convinient for my daughter marriage', 25000, 'Marijuana', 'We make your day.', 'images/halls1.jpg', 'images/halls1.jpg', 'Barat', 'Mehndi'),
(2, 'images/halls3.jpg', '300', 'Eden Town', 6, 'This was the best hall I have ever seen.', 37000, 'Eden Complex ', 'We make you smile', 'images/halls1.jpg', 'images/halls7.jpg', 'Barat', 'We make you smile. '),
(3, 'images/halls2.jpg', '300', 'Model Town', 6, 'This was the best hall I have ever seen.', 15000, 'Gourmet Palace  ', 'We make you smile', 'images/halls11.jpg', 'images/halls12.jpg', 'Barat', 'We make you smile. '),
(4, 'images/halls4.jpg', '400', 'Lal Pul', 6, 'This was the best hall I have ever seen.', 15000, 'Shaheen Palace  ', 'We make you smile', 'images/halls9.jpg', 'images/halls7.jpg', 'Barat', 'We make you smile. '),
(5, 'images/halls5.jpg', '600', 'Lovely Town', 8, 'This was the best hall I have ever seen.', 20000, 'Lovely Complex ', 'We make you smile', 'images/halls4.jpg', 'images/halls7.jpg', 'Barat', 'We make you smile. '),
(6, 'images/halls6.jpg', '200', 'Wonder Town', 4, 'This was the best hall I have ever seen.', 12000, 'Lal pul Complex ', 'We make you smile', 'images/halls1.jpg', 'images/halls12.jpg', 'Barat', 'We make you smile. '),
(7, 'images/halls7.jpg', '200', 'Alo Town', 4, 'This was the best hall I have ever seen.', 12000, 'Pyaza Complex ', 'We make you smile', 'images/halls11.jpg', 'images/halls12.jpg', 'Barat', 'We make you smile. '),
(8, 'images/halls8.jpg', '500', 'Town Ship', 9, 'This hall was beautifull and convinient for my daughter marriage', 25000, 'Marijuana', 'We make your day.', 'images/halls1.jpg', 'images/halls1.jpg', 'Barat', 'Mehndi'),
(9, 'images/halls9.jpg', '500', 'Town Ship', 9, 'This hall was beautifull and convinient for my daughter marriage', 25000, 'Marijuana', 'We make your day.', 'images/halls1.jpg', 'images/halls1.jpg', 'Barat', 'Mehndi');

-- --------------------------------------------------------

--
-- Table structure for table `ourteam`
--

CREATE TABLE IF NOT EXISTS `ourteam` (
  `id` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `img` varchar(50) NOT NULL,
  `details` varchar(200) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `work` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ourteam`
--

INSERT INTO `ourteam` (`id`, `name`, `img`, `details`, `designation`, `work`) VALUES
(1, 'M.Faizan Rafiq', 'images/faizi.jpg', 'Faizan is an overly ambitious individual who loves doing things in a bit rebellious way. Despite being a business major at PUCIT.', 'CO-Founder', 'Student At PUCIT'),
(2, 'Junaid Rasheed', 'images/junaid.jpg', 'Junaid is an overly ambitious individual who loves doing things in a bit rebellious way. Despite being a business major at PUCIT.', 'CO-Founder', 'Student At PUCIT'),
(3, 'Muhammad Awais Sarwar', 'images/dp1.jpg', 'Muhammad Awais is an overly ambitious individual who loves doing things in a bit rebellious way. Despite being a business major at PUCIT.', 'CEO', 'Student At PUCIT'),
(4, 'M.Bilal Shahzad', 'images/sirbilal.jpg', 'Mr. Bilal is an overly ambitious individual who loves doing things in a bit rebellious way. Despite being a business major at PUCIT.', 'Project Manager TRG', 'Assit Prof PUCIT'),
(5, 'M.Haider Ali', 'images/haider.jpg', 'Mr. Haider is an overly ambitious individual who loves doing things in a bit rebellious way. Despite being a business major at PUCIT.', 'Graphic Designer ', 'Student at PUCIT'),
(6, 'M.Adil Farhan', 'images/sirbilal.jpg', 'Mr. Adill is an overly ambitious individual who loves doing things in a bit rebellious way. Despite being a business major at PUCIT.', 'Backend Developer ', 'Student at PUCIT'),
(7, 'M.Asad Jamil', 'images/asad.jpg', 'Mr. Asad is an overly ambitious individual who loves doing things in a bit rebellious way. Despite being a business major at PUCIT.', 'Backend Developer ', 'Student at PUCIT'),
(8, 'M. Abdulrehman ', 'images/mirza.jpg', 'Mr. Abdulrehman  is an overly ambitious individual who loves doing things in a bit rebellious way. Despite being a business major at PUCIT.', 'Backend Developer ', 'Student at PUCIT');

-- --------------------------------------------------------

--
-- Table structure for table `photographer`
--

CREATE TABLE IF NOT EXISTS `photographer` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rate` int(100) NOT NULL,
  `rating` int(100) NOT NULL,
  `images` varchar(200) NOT NULL,
  `review` varchar(100) NOT NULL,
  `slideImg1` varchar(100) NOT NULL,
  `slideImg2` varchar(100) NOT NULL,
  `slidecont1` varchar(100) NOT NULL,
  `slidecont2` varchar(100) NOT NULL,
  `nameDj` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photographer`
--

INSERT INTO `photographer` (`id`, `name`, `rate`, `rating`, `images`, `review`, `slideImg1`, `slideImg2`, `slidecont1`, `slidecont2`, `nameDj`) VALUES
(1, 'Mama Gama Studios', 2000, 9, 'images/camera.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Junaid JJ DJ'),
(2, 'Lama Gama Studios', 2000, 6, 'images/camera2.png', 'His sound system was awesome. ', 'images/photoimg1.jpg', 'images/photoimg.jpg', 'We capture your memories', 'We capture your memories', 'Awais DJ'),
(3, 'Tama Gama Studios', 1500, 5, 'images/camera3.png', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'JJ DJ'),
(4, 'Alo Studios', 1500, 5, 'images/camera6.png', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'JJ DJ'),
(5, 'Amma Gama Studios', 2000, 9, 'images/camera5.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Junaid DJ'),
(6, 'Mama Gama Studios', 2000, 9, 'images/camera7.png', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Junaid JJ DJ'),
(7, 'Gama Studios', 2000, 9, 'images/camera8.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Faizi Dj'),
(8, 'Gama Studios', 2000, 9, 'images/camera9.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Annoy Dj'),
(9, 'Gama Studios', 2000, 9, 'images/camera10.jpg', 'His sound system was awesome. ', 'images/photoimg.jpg', 'images/photoimg1.jpg', 'We capture your memories', 'We capture your memories', 'Dj Mewao');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE IF NOT EXISTS `testimonial` (
  `id` int(20) NOT NULL,
  `client` varchar(20) NOT NULL,
  `date` date DEFAULT NULL,
  `comment` varchar(1000) NOT NULL DEFAULT 'Trioplanners provides satisfactory services .  ',
  `rating` int(1) NOT NULL DEFAULT '5' COMMENT 'rating out of 5.'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `client`, `date`, `comment`, `rating`) VALUES
(1, 'Mobilink', '0000-00-00', 'We want to thank you all you guys specially the master… the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 4),
(2, 'Mobilink', '0000-00-00', 'We want to thank you all you guys specially the master… the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 4),
(3, 'PUCIT', '0000-00-00', 'We want to thank you all you guys specially the master… the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 4),
(4, 'Junaid', '0000-00-00', 'We want to thank you all you guys specially the master… the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 3),
(5, 'Awais', '0000-00-00', 'We want to thank you all you guys specially the master… the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 5),
(6, 'Asad', '0000-00-00', 'We want to thank you all you guys specially the master… the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 3),
(7, 'Ali', '0000-00-00', 'We want to thank you all you guys specially the master… the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 3),
(8, 'saad', '2015-12-10', 'the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 3),
(9, 'Waqar', '2015-12-11', 'one DJSHAANO on our wedding ceremony. Your ability to work wonder the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 3),
(10, 'Bilal', '2015-12-20', 'You are truly gifted in knowing people and creating a perfect  one DJSHAANO on our wedding ceremony. Your ability to work wonder the only one DJSHAANO on our wedding ceremony. Your ability to work wonders on the crowd was amazing. You are truly gifted in knowing people and creating a perfect atmosphere. Really GREAT!', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caterers`
--
ALTER TABLE `caterers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `decorators`
--
ALTER TABLE `decorators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dj`
--
ALTER TABLE `dj`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `hall`
--
ALTER TABLE `hall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ourteam`
--
ALTER TABLE `ourteam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photographer`
--
ALTER TABLE `photographer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `caterers`
--
ALTER TABLE `caterers`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `decorators`
--
ALTER TABLE `decorators`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `dj`
--
ALTER TABLE `dj`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `hall`
--
ALTER TABLE `hall`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ourteam`
--
ALTER TABLE `ourteam`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `photographer`
--
ALTER TABLE `photographer`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
